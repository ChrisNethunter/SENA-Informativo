/**
 * Colección encargada de almacenar los documentos con la información de la 
 * entidad que hara uso del sistema
 * @constructor
 * @author Christian David León Jiménez
 * @version 0.1
*/

Entity = new Meteor.Collection('entity');

