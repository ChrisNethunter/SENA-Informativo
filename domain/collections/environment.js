/**
 * Colección encargada de almacenar los documentos de cada uno de los ambientes
 * @constructor
 * @author Christian David León Jiménez
 * @version 0.1
*/
Enviroments = new Meteor.Collection('environments');
