/**
 * Colección encargada de almacenar los documentos de cada uno de los eventos
 * generados en el sistema
 * @constructor
 * @author Christian David León Jiménez
 * @version 0.1
*/
Events = new Meteor.Collection('events');

