/**
 * Colección encargada de almacenar las imágenes institucionales
 * @constructor
 * @author Christian David León Jiménez
 * @version 0.1
*/
Images = new Meteor.Collection('images');

var ImagesStoreFileSystem = new FS.Store.FileSystem("ImagesFileSystem", {});

Images = new FS.Collection("images", {
    stores: [ImagesStoreFileSystem]
});
Images.allow({
    insert: function(userId, doc) {
      return (userId && doc.metadata.owner === userId);
    },
    update: function(userId, doc, fieldNames, modifier) {
      return (userId === doc.metadata.owner);
    },
    remove: function(userId, doc) {
      return (userId && doc.metadata.owner === userId);
    },
    download: function(userId) {
      return true;
    }
});
