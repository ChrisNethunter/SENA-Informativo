/**
 * Colección encargada de almacenar los documentos de cada una de las noticias
 * @constructor
 * @author Christian David León Jiménez
 * @version 0.1
*/
Informative = new Meteor.Collection('informative');