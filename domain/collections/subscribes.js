/**
 * Colección encargada de almacenar los documentos de cada uno de los programas
 * @constructor
 * @author Christian David León Jiménez
 * @version 0.1
*/
Subscribes = new Meteor.Collection('subscribes');