Perms = {

  isAdminById: function(userId) {
    var user = Meteor.users.findOne(userId);
    return user && user.isAdmin;
  },

  isDocumentOwnerByIdOrAdmin: function(userId, doc) {
    return Perms.isDocumentOwnerById(userId, doc) || Perms.isAdminById(userId);
  },

  isDocumentOwnerById: function(userId, doc) {
    return userId === doc.user;
  },

   isDocumentOwnerByIdUpdate: function(userId, doc) {
    return userId === doc.user;
  },

  removeForbidden: function() {
    return true;
  },
  updateDocumentOwnerById: function(userId, docs, fields, modifier) {
      for(var i=0; i<docs.length; i++ ){
                  if ( docs[i].user != userId) {
                              return false;
                  }
      }

      return true;
  },
  insertCollectionFsOwnerById : function(userId, file) {             
        return userId && file.owner === userId; 
  },

  updateCollectionFsOwnerById :function(userId, files, fields, modifier) {
        return _.all(files, function (file) {
            return (userId == file.owner);
        });  //EO iterate through files
  },

  removeCollectionFsOwnerById:function(userId, files) { 
      return false; 
  },
  downloadCollectionFsOwnerById:function () {
      return true;
  }

};
