/**
 * Colección encargada de almacenar los documentos de cada una de las noticias
 * generadas por parte del administrador
 * @constructor
 * @author Christian David León Jiménez
 * @version 0.1
*/
Notices = new Meteor.Collection('notices');

