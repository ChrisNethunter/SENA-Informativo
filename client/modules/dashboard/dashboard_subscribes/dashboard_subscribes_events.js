Template.dashboard_subscribes.rendered = function(){
    $(".content-users").fadeIn("slow");
}

Template.dashboard_subscribes.events({
	 'click .remove-client': function (event) {
      event.preventDefault();
      sweetAlert({
          title: "Estas seguro?",
          text: "Usted no será capaz de recuperar esta información!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si, cancelar suscripción!",
          closeOnConfirm: false
      }, function() {
          Meteor.call('removeSubscribe',event.currentTarget.id, function(err, res){
            if (err){
              sweetAlert('Oops...', 'Intenta de Nuevo!', 'error');
            }else{
              sweetAlert("Buen Trabajo!", "Se ha cancelado!", "success");
            };
          });    
      });
    },
    
});

