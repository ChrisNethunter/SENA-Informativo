Template.dashboard_subscribes.helpers({
    showSubscribes: function(object) {
       return Subscribes.find();
    },
    showSubscribesCount: function(object) {
       return Subscribes.find().count();
    },
    tableSettingsSubscribes : function () {
        return  {
                    rowsPerPage: 15,
                    fields: [
                        { 
                            key: 'email', label: 'Emails' 
                        },
                        { 
                            key: 'createAt', label: 'Fecha de Creación' 
                        },
                        { 
                            key: 'nameEnviroment', 
                            label: 'Ambiente',

                        },
                        { 
                            key: 'nameProgram', 
                            label: 'Programa',
                            /*fn: function (program, object) {
                                Meteor.subscribe("programDetail",program)
                                var data = Programs.findOne({_id:program})
                                var html =  '<td>'+ data.name +'</td>';
                                console.clear()
                                return new Spacebars.SafeString(html);

                            }*/
                        },
                        { 
                            key: '_id', 
                            label: 'Opciones',
                            fn: function (_id, object) {
                                var html =  '<button id="'+_id+'" class="remove-client btn btn-danger btn-flat">Cancelar Subscripción</button>';
                                return new Spacebars.SafeString(html);
                            }
                        },     
                    ],
            };
    }

});
Template.dashboard_subscribes.onCreated(function () {
    Meteor.subscribe('susbcritions')
    
});