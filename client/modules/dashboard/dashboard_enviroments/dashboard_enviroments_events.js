Template.dashboard_enviroments.events({
	'click #add-program': function (event) {
		event.preventDefault();
		$("#content-table").slideUp("slow");
		$("#add-enviroment-form").slideDown("slow");
    },
    'click #cancel-add': function (event) {
    	event.preventDefault();
    	$("#add-enviroment-form").slideUp("slow");
        $("#edit-enviroment-form").slideUp("slow");
		$("#content-table").slideDown("slow");		
    },
    'submit #add-enviroment-form': function(event,template) {
        
        event.preventDefault();
        var date = dateString()
        var data = 	{	
        				name:$('#name').val(),
                        video:$('#url-video').val(),
        				description:$('#description').val(),
        				user:Meteor.userId(),
        				createAt:dateString()["stringDate"],
                        day: date["day"],
                        month: date["month"], 
                        year: date["year"],
                        time: date["time"],
        				img:'',
        				status:true,
        				
    				}
    	addEnviroment(data)
    },
    'submit #edit-enviroment-form': function(event,template) {
        
        event.preventDefault();
        var data =  {   
                        _id:$("#id-enviroment-edit").val(),
                        name:$('#edit-name').val(),
                        video:$('#edit-url-video').val(),
                        description:$('#edit-description').val(),
                        dateModify :dateString()["stringDate"],
                        img:'',
                        user:Meteor.userId(),
                        status:true,
                    }
        editEnviroment(data)
    },
    'click .update-enviroment': function (event) {
        event.preventDefault();
        $('.alert').remove()
        $("#content-table").slideUp("slow");
        $("#edit-enviroment-form").slideDown("slow");
        loadDataEdit(event.currentTarget.id)
    }
})

Template.dashboard_enviroments.rendered = function(){
    setTimeout(function(){ 
		$('.alert').remove()
	}, 500);

}


function addEnviroment(data){
	$('.alert').remove()
    var fileImg = $('#img').get(0).files[0];
    var video_id = cutUrlDataYoutube(data.video);

    if (LimitAttach($('#img').val(),1) && isNotEmpty("#name",data.name) &&  
        isNotEmpty("#description",data.description) && video_id.status == true )
    {
        

        console.log("init data img ");

		/*Upload Img CollectionFS*/
	    var newFileImg = new FS.File(fileImg);
	    newFileImg.metadata = {
	        owner: Meteor.userId()
	    };
	    var fileObjImg = Images.insert(newFileImg, function (err, fileObj) {});
	    /*Upload Img CollectionFS*/

	    data.img = fileObjImg;
        data.idVideoYoutube = video_id.videoId;

	    Meteor.call('insertEnviroment',data, function(err, res){
	        if (err){
                sweetAlert("Ops..!", "Error, Intenta de Nuevo", "error")      
	        }
	    });    
	           
    }else{
        sweetAlert("Ops..!", "Rellena todos los campos correctamente", "error")  
    }
}

function loadDataEdit(id){
    var data = Enviroments.findOne({_id:id});

    $("#edit-name").val(data.name);
    $("#edit-url-video").val(data.video);
    $("#edit-description").val(data.description)
    $("#edit-img-preview").attr("src","/cfs/files/images/"+data.img._id+"/"+data.img.original.name)
    $("#id-img-edit").val(data.img._id);
    $("#id-enviroment-edit").val(id)
}

function editEnviroment(data){
    $('.alert').remove()
    
    var video_id = cutUrlDataYoutube(data.video);
    if (isNotEmpty("#name",data.name) &&  isNotEmpty("#description",data.description) && video_id.status == true )
    {
        data.idVideoYoutube = video_id.videoId;
        if ($('#edit-img').val() != '' ){
            if (LimitAttach($('#edit-img').val(),1) == true){

                var fileImg = $('#edit-img').get(0).files[0];
                /*Upload Img CollectionFS*/
                var newFileImg = new FS.File(fileImg);
                newFileImg.metadata = {
                    owner: Meteor.userId()
                };
                var fileObjImg = Images.insert(newFileImg, function (err, fileObj) {});
                /*Upload Img CollectionFS*/

                data.img = fileObjImg;

                Meteor.call('updateEnviroment',data, function(err, res){
                if (err){
                    sweetAlert("Ops..!", "Error, Intenta de Nuevo", "error")          
                }
            });

            }else{
                sweetAlert("Ops..!", "Rellena todos los campos correctamente", "error")
            };
        }else{
            Meteor.call('updateEnviroment',data, function(err, res){
                if (err){
                    sweetAlert("Ops..!", "Error, Intenta de Nuevo", "error")          
                }else{
                    sweetAlert("Good job!", "Se han guardado los datos con éxito!", "success")
                }
            });
        }       
    }else{
        sweetAlert("Ops..!", "Rellena todos los campos correctamente", "error")
    }
}