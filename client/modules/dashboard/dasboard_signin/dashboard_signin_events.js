Template.dashboard_signin.events({
    'submit #signin-form_id' : function(event, t){
        event.preventDefault();
        $(".alert").remove();
        // retrieve the input field values
        var email = t.find('#username_id').value
        , password = t.find('#password_id').value;

        if (isNotEmpty('#username_id',email),isNotEmpty('#password_id',password) === true ){
          // Trim and validate your fields here.... 
          // If validation passes, supply the appropriate fields to the
          // Meteor.loginWithPassword() function.
          Meteor.loginWithPassword(email, password, function(err,res){
            if (err){
              if (err.message === 'User not found [403]') {
                var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Usuario No encontrado</h4>';
                $(liData).appendTo('.form-actions').fadeIn('slow');
              }else{
                var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Contraseña Incorrecta</h4>';
                $(liData).appendTo('.form-actions').fadeIn('slow');
              }
            }else{
              
              Router.go('dashboard_control');
              // The user has been logged in.
              // The user might not have been found, or their passwword
              // could be incorrect. Inform the user that their
              // login attempt has failed. 
            } 
          });
        }else{
          var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;" align="center">Formulario, Vacio</h4>';
          $(liData).appendTo('.form-actions').slideDown();
        };
        
         return false; 
    },

    'click #sign-up' : function(event, t){
      event.preventDefault();
      Router.go('singUp');    
    },
    
    'click #forgot-password-link' : function(event, t){
        $('#password-reset-form').fadeIn(400);
        return false;
    },

    'click #password-reset-form .close' : function(event, t){
        $('#password-reset-form').fadeOut(400);
        return false;
    },

    'submit #password-reset-form_id': function(e, t) {
        $(".alert").remove();
        e.preventDefault();

        var forgotPasswordForm = $(e.currentTarget),
            email = trimInput(forgotPasswordForm.find('#p_email_id').val());

        if (isNotEmpty(email) && validateEmail(email)) {

            Accounts.forgotPassword({email: email}, function(err) {
              if (err) {
                if (err.message === 'User not found [403]') {
                  var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Este email no existe.</h4>';
                  $(liData).appendTo('.form-actions').fadeIn('slow');
                } else {
                  var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;" align="center"Lo sentimos algo malo paso.</h4>';
                $(liData).appendTo('login-buttons-reset-password-button').slideDown();
                }
              } else {
                  var liData = '<h4 id="alert" align="center" class="alert" style="color:green;display:none;">Email enviado, verifica tu bandeja de entrada.</h4>';
                  $(liData).appendTo('.form-actions').fadeIn('slow');
              }
            });
            /*reset password with method no works =s
            Meteor.call('resetPassword',email, function(err, res){
              
              if(res == 1){
                  var liData = '<h4 id="alert" align="center" class="alert" style="color:green;display:none;">Email Sent. Check your mailbox.</h4>';
                  $(liData).appendTo('.form-actions').fadeIn('slow');
              }else if (res == 3){
                  var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">This email does not exist.</h4>';
                  $(liData).appendTo('.form-actions').fadeIn('slow');
              }else{
                var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;" align="center">We are sorry but something went wrong.</h4>';
                $(liData).appendTo('login-buttons-reset-password-button').slideDown();
              }
            });*/
        }
        return false;

    },
    'click .close' : function(event, t){
        event.preventDefault();
        Router.go('signIn');    
    },
    /*New Password*/

    'submit #password-new-form': function(e, t) {
        e.preventDefault();
        $(".alert").remove();

        var password = t.find('#reset-password-new-password');
            //passwordConfirm = resetPasswordForm.find('#resetPasswordPasswordConfirm').val();

        if (isNotEmpty(password) && validatePassword(password)  === true) {
            Meteor.call('newPassword',password, function(err, res){
              
              if(res == 1){
                var liData = '<h4 id="alert" align="center" class="alert" style="color:green;display:none;">Your password has been changed. Welcome back!</h4>';
                $(liData).appendTo('.form-actions').fadeIn('slow');
              }else{
                var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;" align="center">We are sorry but something went wrong.</h4>';
                $(liData).appendTo('login-buttons-reset-password-button').slideDown();
              }
            });
        }
        return false;
    },

    'click #facebook-login': function(event) {
        Meteor.loginWithFacebook({}, function(err){
            if (err) {
                throw new Meteor.Error("Facebook login failed");
            }
        });
    },

    'click #twitter-login': function(event) {
        Meteor.loginWithTwitter({}, function(err){
            if (err) {
                throw new Meteor.Error("Twitter login failed");
            }
        });
    },

    
});

trimInput = function(value) {
  return value.replace(/^\s*|\s*$/g, '');
};


if (Accounts._resetPasswordToken) {
    Session.set('resetPassword', Accounts._resetPasswordToken);
}

Template.dashboard_signin.rendered = function(){
	
}
