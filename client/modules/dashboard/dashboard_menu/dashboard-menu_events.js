Template.dashboard_menu.events({
    'click #userAccount': function (event) {
        event.preventDefault();
        Router.go('dashboardUserProfile');    
    },
    'click #logout': function(event){
        event.preventDefault();
        Meteor.logout();
    },
    'click #index': function (event) {
        event.preventDefault();
        Router.go('dashboard');    
    },
    'click #dashboard-institute': function (event) {
        event.preventDefault();
        Session.set("asideOpt","dashboard_institute")
        Router.go('dashboardInstitute');    
    },
    'click #programs': function (event) {
        event.preventDefault();
        Session.set("asideOpt","dashboard_programs")
        Router.go('dashboardPrograms');    
    },
    'click #news': function (event) {
        event.preventDefault();
        Session.set("asideOpt","dashboard_news")
        Router.go('dashboardNews');    
    },
    'click #events': function (event) {
        event.preventDefault();
        Session.set("asideOpt","dashboard_events")
        Router.go('dashboardEvents');    
    },
    'click #enviroments': function (event) {
        event.preventDefault();
        Session.set("asideOpt","dashboard_enviroments")
        Router.go('dashboardEnviroments');    
    },
    'click #userSettings': function (event) {
        event.preventDefault();
        Router.go('dashboardUserSettings');    
    },
    'click #subscribes': function (event) {
        event.preventDefault();
        Session.set("asideOpt","dashboard_subscribes")
        Router.go('dashboardSubscribes');    
    },
    'click #informative-emails': function (event) {
        event.preventDefault();
        Session.set("asideOpt","dashboard_informatives_emails")
        Router.go('dashboardInformativeEmails');    
    },
    'click #user-roles': function (event) {
        event.preventDefault();
        Session.set("asideOpt","dashboard_users_roles")
        Router.go('dashboardUsersRoles');    
    },
    'click #main-menu-toggle': function(event){
        event.preventDefault();
        if ($('body').hasClass("mmc")){
            $('body').removeClass( "mmc" )
            $('body').addClass('mme');
        }else{
            $('body').removeClass( "mme" )
            $('body').addClass('mmc');
        };
    },
});

Template.dashboard_menu.rendered = function(){ 


}

