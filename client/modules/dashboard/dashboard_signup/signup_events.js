if (Meteor.isClient) {
  // counter starts at 0
	Template.signUp.events({
	'click #signIn' : function(event, t){
	  event.preventDefault();
	  Router.go('login');    
	},

	'submit #signup-form_id' : function(event, template) {
	  	event.preventDefault();
	  	$(".alert").remove();

	  	var email = validateEmail(template.find('#email_id').value);
	  	var userPassword = validatePassword(template.find('#password_id'));
	  	var username = isNotEmpty(template.find('#name_id').value);
	  	var fullname = isNotEmpty(template.find('#username_id').value);

	  
	  	if ( userPassword &&  email && username && fullname === true ){// &amp;&amp; other validations) {
			var data =  {
		      				email:template.find('#email_id').value,
		      				password:template.find('#password_id').value,
		      				profile:{
		      					fullname:template.find('#name_id').value,
		      					username:template.find('#username_id').value,
		      				}
		      				
		      			};


		    Meteor.call('createUserWithRole',data, function(err, res){
		  		//alert("email " + res.email +"  pass "+ res.password + " fullname "+ res.fullname + " username " + res.username)

			    if(res.length){
			      	var liData = '<h4 id="alert" align="center" class="alert" style="color:green;display:none;">Registered</h4>';
      			  	$(liData).appendTo('.form-actions').fadeIn('slow');
			    }else if (res == 3){
			       	var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Email already exists</h4>';
      			  	$(liData).appendTo('.form-actions').fadeIn('slow');
			    }else{
			    	var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;" align="center">Problems, Try Again</h4>';
      			  	$(liData).appendTo('.form-actions').slideDown();
			    }
			});
			// Then use the Meteor.createUser() function*/
		}else{
			var liData = '<h4 id="alert" class="alert" style="color:red;display:none;" align="center">Fill in all fields</h4>';
      		$(liData).appendTo('.form-actions').slideDown();
		}
	
	  	return false;
	}

		

	});

	Template.signUp.rendered = function(){
		 $("body").addClass("page-signup");

	}

}


