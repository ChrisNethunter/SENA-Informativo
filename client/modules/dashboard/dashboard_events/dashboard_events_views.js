Template.dashboard_events.helpers({
    showEvents: function(object) {
       return Events.find({type:"event"});
    },
    showEventsCount: function(object) {
       return Events.find({type:"event"}).count();
    },
    showEnviroments: function(object) {
       return Enviroments.find();
    },
    showPrograms:function(object){
        return Programs.find();
    },
    showImages:function(object){
        return Images.find();
    },
    tableSettingsEvents : function () {
        return {
                    rowsPerPage: 15,
                    fields: [
                        { 
                            key: 'img', 
                            label: 'Imagen',
                            fn: function (img, object) {
                                var img = "/cfs/files/images/"+img._id+"/"+img.original.name
                                var html =  '<img class="preview-img" id="img-enviroment" src="'+img+'">';
                                return new Spacebars.SafeString(html);
                            }
                        },
                        { 
                            key: 'name', 
                            label: 'Título' 
                        },
                        /*{ 
                            key: 'nameEnviroment', 
                            label: 'Ambiente' 
                        },
                        { 
                            key: 'nameProgram', 
                            label: 'Programa' 
                        },*/
                        { 
                            key: 'description', 
                            label: 'Descripción' 
                        },
                        { 
                            key: 'dateCreated', 
                            label: 'Fecha de Creación' 
                        },
                        { 
                            key: 'status', 
                            label: 'Estado',
                            fn: function (estado, object) {
                                if (estado == "activo"){
                                    var html =  '<div id="true" class="btn btn-success btn-flat">Activo</div>';
                                    
                                }else{
                                    var html =  '<div id="false" class="btn btn-danger btn-flat">Inactivo</div>';
                                };
                                return new Spacebars.SafeString(html);
                                
                            }
                        },
                        { 
                            key: '_id', 
                            label: 'Opciones',
                            fn: function (_id, object) {
                                var html =  '<button id="'+_id+'" class="edit-event btn btn-info btn-flat">Modificiar</button>';
                                return new Spacebars.SafeString(html);
                            }
                        },     
                    ],
        };
    }
});
Template.dashboard_events.onCreated(function () {
    Meteor.subscribe('dataNews',"event",16)
    Meteor.subscribe('programsLimit',70)
    Meteor.subscribe('enviromentsLimit',15)
    Meteor.subscribe("Images", Meteor.userId())
});