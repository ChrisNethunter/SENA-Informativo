Template.dashboard_events.rendered = function(){
    Session.set("skip",16)
    Meteor.call('countDataNews',function(error, result) {
        if (!error) { 
            if (result > 100000){  
                setTimeout(
                    function(){
                        subscription = Meteor.subscribe("dataNews","event",Session.get("skip"))
                        $('.alert').remove()
                    },2000
                )

            }else{ 
                setTimeout(
                    function(){
                        subscription = Meteor.subscribe("dataNews","event",Meteor.userId(),Session.get("skip"))
                        $('.alert').remove()
                    },500
                )
            } ;
        }
    });
}

Template.dashboard_events.events({
	'click #add-event': function (event) {
		event.preventDefault();
		$("#content-table").slideUp("slow");
		$("#add-new-form").slideDown("slow");
    },
    'click #cancel-add': function (event) {
    	event.preventDefault();
        $("#edit-new-form").slideUp("slow");
    	$("#add-new-form").slideUp("slow");
		$("#content-table").slideDown("slow");
    },
    'submit #add-new-form': function(event,template) {
        
        event.preventDefault();
        var date = dateString()
        var data =  {   
                        name:$('#name').val(),
                        video:$('#url-video').val(),
                        description:$('#description').val(),
                        user:Meteor.userId(),
                        createAt:dateString()["stringDate"],
                        day: date["day"],
                        month: date["month"], 
                        year: date["year"],
                        time: date["time"],
                        type:"event",
                        img:'',
                        status:"activo",
                    }

        if ($('#enviroment').val() != '' || $('#program').val() != '' ){
            data.enviroment = $('#enviroment').val()
            data.nameEnviroment = $("#enviroment option:selected").text()
            data.program = $('#program').val()
            data.nameProgram = $("#program option:selected").text()
        };
    	addEvent(data)
    },
    'click .next-page' : function(event,template){
        event.preventDefault();
        Session.set("skip",Session.get("skip") + 15)
        Meteor.subscribe("dataNews","event",Session.get("skip"))
    },
    'submit #edit-new-form': function(event,template) {
        event.preventDefault();
        var date = dateString()
        var data =  {   
                        _id:$("#id-new-edit").val(),
                        name:$('#name-edit').val(),
                        video:$('#url-video-edit').val(),
                        description:$('#description-edit').val(),
                        user:Meteor.userId(),
                        dateModify:dateString()["stringDate"],
                        img:'',
                        
                    }

        if ($('#enviroment-edit').val() != '' || $('#program-edit').val() != '' ){

            data.enviroment = $('#enviroment-edit').val()
            data.nameEnviroment = $("#enviroment-edit option:selected").text()
            
            data.program = $('#program').val()
            data.nameProgram = $("#program-edit option:selected").text()

        };
        editNew(data)
    },
    'click .edit-event': function (event) {
        event.preventDefault();
        $('.alert').remove()
        $("#content-table").slideUp("slow");
        $("#edit-new-form").slideDown("slow");
        loadDataEdit(event.currentTarget.id)
    }
    
});

function addEvent(data){
	$('.alert').remove()
    var fileImg = $('#img').get(0).files[0];
    var video_id = cutUrlDataYoutube(data.video);

    if (LimitAttach($('#img').val(),1) && isNotEmpty("#name",data.name) && isNotEmpty("#description",data.description)
    	&& video_id.status == true )
    {
        
		/*Upload Img CollectionFS*/
	    var newFileImg = new FS.File(fileImg);
	    newFileImg.metadata = {
	        owner: Meteor.userId()
	    };
	    var fileObjImg = Images.insert(newFileImg, function (err, fileObj) {});
	    /*Upload Img CollectionFS*/

	    data.img = fileObjImg;
        data.idVideoYoutube = video_id.videoId;

	    Meteor.call('insertNews',data, function(err, res){
	        if (err){
	            var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Error, Intenta mas tarde</h4>';
	            $(liData).appendTo('.col-sm-offset-2').slideDown('slow');      
	        }
	    });
	           
    }else{
        var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">LLena todos los Campos Correctamente</h4>';
                    $(liData).appendTo('.col-sm-offset-2').slideDown('slow');    
    }
}

function editNew(data){
    $('.alert').remove()
    
    var video_id = cutUrlDataYoutube(data.video);
    if (isNotEmpty("#name-edit",data.name) &&  isNotEmpty("#description-edit",data.description)
        && video_id.status == true )
    {
        data.idVideoYoutube = video_id.videoId;
        if ($('#img-edit').val() != '' ){
            if (LimitAttach($('#img-edit').val(),1) == true){

                var fileImg = $('#img-edit').get(0).files[0];
                /*Upload Img CollectionFS*/
                var newFileImg = new FS.File(fileImg);
                newFileImg.metadata = {
                    owner: Meteor.userId()
                };
                var fileObjImg = Images.insert(newFileImg, function (err, fileObj) {});
                /*Upload Img CollectionFS*/

                data.img = fileObjImg;
                Meteor.call('updateNew',data, function(err, res){
                if (err){
                    sweetAlert("Ops..!", "Error, Intenta de Nuevo", "error")          
                }
            });

            }else{
                sweetAlert("Ops..!", "Rellena todos los campos correctamente", "error")
            };
        }else{
            Meteor.call('updateNew',data, function(err, res){
                if (err){
                    sweetAlert("Ops..!", "Error, Intenta de Nuevo", "error")          
                }else{
                    sweetAlert("Buen Trabajo!", "Se han guardado los datos con éxito!", "success")
                }
            });
        }       
    }else{
        sweetAlert("Ops..!", "Rellena todos los campos correctamente", "error")
    }
}

function loadDataEdit(id){
    var data = Events.findOne({_id:id});

    $("#name-edit").val(data.name);
    $("#url-video-edit").val(data.video);
    $("#description-edit").val(data.description)
    $("#edit-img-preview").attr("src","/cfs/files/images/"+data.img._id+"/"+data.img.original.name)
    $("#id-img-edit").val(data.img._id);
    $("#id-new-edit").val(data._id)

    if (data.enviroment || data.enviroment ){
        $("#enviroment-edit").val(data.enviroment)
        $("#program-edit").val(data.program)
    };
}