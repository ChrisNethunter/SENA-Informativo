Template.dashboard_institute.events({
	'click #signIn' : function(event, template){
	  event.preventDefault();
	  Router.go('dashboard');    
	},
	'submit #info-institute': function(event,template) {
        //insertUrlVideoYoutube($("#inputUrlVideo").val() , $("#inputNameVideoYouTube").val(), $("#inputCategorieVideoYouTube").val() ,$("#inputCompanyVideoVideoYouTube").val(),$("#inputDescriptionVideoYouTube").val());
        event.preventDefault();
        var date = dateString()
        var data = 	{	
        				name:$('#name').val(),
        				email:$('#email').val(),
        				phone:$('#phone').val(),
        				address:$('#address').val(),
        				description:$('#description').val(),
        				img:'',
        				createAt:dateString()["stringDate"],
                        day: date["day"],
                        month: date["month"], 
                        year: date["year"],
                        time: date["time"],
        				user:Meteor.userId(),
    				}
        if ($('#id-entity').val() == '' ){
        	data.status = true;
        	InfoInstitute(data);
        }else{
        	editInfoInstitute($("#id-entity").val(),data);
        };
        
    },

});

Template.dashboard_institute.rendered = function(){
	
	setTimeout(function(){ 
		$('.alert').remove()
	}, 500);
}

/*
	Function add info institute on collection Institutes
*/
function InfoInstitute(data){
	$('.alert').remove()
    var fileImg = $('#img').get(0).files[0];

    if (LimitAttach($('#img').val(),1) && isNotEmpty("#name",data.name) && isNotEmpty("#description",data.description) &&
    	validateNumber('#phone',data.phone) && isNotEmpty("#address",address) == true )
    {
        
		/*Upload Img CollectionFS*/
	    var newFileImg = new FS.File(fileImg);
	    newFileImg.metadata = {
	        owner: Meteor.userId()
	    };
	    var fileObjImg = Images.insert(newFileImg, function (err, fileObj) {});
	    /*Upload Img CollectionFS*/

	    data.img = fileObjImg;

	    Meteor.call('insertInfoInstitute',data, function(err, res){
	        if (err){
	        	sweetAlert("Ops..!", "Error, Intenta de Nuevo", "error")
	            /*var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Error, Try Again</h4>';
	            $(liData).appendTo('.col-sm-offset-2').slideDown('slow');*/         
	        }
	    });    
	           
    }else{
    	sweetAlert("Ops..!", "Rellena todos los campos correctamente", "error")
        /*var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Fill all fields</h4>';
                    $(liData).appendTo('.col-sm-offset-2').slideDown('slow');*/    

    }
}

function editInfoInstitute(id,data){
	$('.alert').remove()


	if (isNotEmpty("#name",data.name) && isNotEmpty("#description",data.description) &&
    	validateNumber('#phone',data.phone) && isNotEmpty("#address",address) == true) {

		if ($('#img').val() == '' ){

			Meteor.call('editInfoInstitute',id,data, function(err, res){
		        if (err){
		        	 sweetAlert("Ops..!", "Error, Intenta de Nuevo", "error")
		            /*var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Error, Intenta de Nuevo</h4>';
		            $(liData).appendTo('.col-sm-offset-2').slideDown('slow');*/          
		        }else{
		        	sweetAlert("Good job!", "Se han guardado los datos con éxito!", "success")
		        	/*var liData = '<h4 id="alert" align="center" class="alert" style="color:green;display:none;">Se han guardado los datos con éxito</h4>';
		            $(liData).appendTo('.col-sm-offset-2').slideDown('slow'); */
		        }
		    });

		}else{

		    Meteor.call('removeImgInstitute',$('#id-img').val(), function(err, res){
		        if (err){
		        	sweetAlert("Ops..!", "Error, Intenta de Nuevo", "error")
		            /*var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Error, Intenta de Nuevo</h4>';
		            $(liData).appendTo('.col-sm-offset-2').slideDown('slow');*/          
		        }else{
		        	var fileImg = $('#img').get(0).files[0];

					/*Upload Img CollectionFS*/
				    var newFileImg = new FS.File(fileImg);
				    newFileImg.metadata = {
				        owner: Meteor.userId()
				    };
				    var fileObjImg = Images.insert(newFileImg, function (err, fileObj) {});
				    /*Upload Img CollectionFS*/

		    		data.img = fileObjImg;
		    		
		        	Meteor.call('editInfoInstitute',id,data, function(err, res){
				        if (err){
				        	sweetAlert("Ops..!", "Error, Intenta de Nuevo", "error")
				            /*var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Se han guardado los datos con éxito</h4>';
				            $(liData).appendTo('.col-sm-offset-2').slideDown('slow');*/          
				        }
				    });
		        }
		    });


		};	
	}else{
		sweetAlert("Ops..!", "Rellena todos los campos correctamente", "error")
        /*var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Fill all fields</h4>';
                    $(liData).appendTo('.col-sm-offset-2').slideDown('slow');*/    

    };

}
