Template.dashboard_programs.helpers({
    showEnviroments: function(object) {
       return Enviroments.find();
    },
    showInfoPrograms:function(object){
    	return Programs.find();
    },
    numberPrograms:function(object){
    	return Programs.find().count();
    },
    showImages:function(object){
    	return Images.find();
    },
    tableSettings : function () {
	    return {
	                rowsPerPage: 15,
	                fields: [
	                    { 
                            key: 'img', 
                            label: 'Imagen',
                            fn: function (img, object) {
                            	var img = "/cfs/files/images/"+img._id+"/"+img.original.name
                                var html =  '<img class="preview-img" id="img-enviroment" src="'+img+'">';
                                return new Spacebars.SafeString(html);
                            }
                        },
	                    { 
	                        key: 'name', label: 'Nombre' 
	                    },
	                    { 
	                        key: 'video', label: 'Video' 

	                    },
	                    { 
	                        key: 'description', label: 'Descripción' 

	                    },
	                    { 
                            key: 'status', 
                            label: 'Estado',
                            fn: function (estado, object) {
                            	if (estado == true){
                            		var html =  '<div id="true" class="btn btn-success btn-sm">Activo</div>';
                            		
                            	}else{
                            		var html =  '<div id="false" class="btn btn-danger btn-sm">Inactivo</div>';
                            	};
                            	return new Spacebars.SafeString(html);                   
                            }
                        },
	                    {
	                        key:'createAt' ,label:'Fecha de Creación'
	                    },
	                    { 
                            key: '_id', 
                            label: 'Opciones',
                            fn: function (_id, object) {
                                var html =  '<button id="'+_id+'" class="update-program btn btn-info btn-flat">Modificar</button>';
                                return new Spacebars.SafeString(html);
                            }

                        },
	                   
	                ],

        };
    }
});

Template.dashboard_programs.onCreated(function () {
	Meteor.subscribe("programs",Meteor.userId())
	Meteor.subscribe("enviroments")
	Meteor.subscribe("Images", Meteor.userId())
	Meteor.subscribe("users")

})

	
