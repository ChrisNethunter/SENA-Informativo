Template.dashboard_programs.events({
	'click #add-program': function (event) {
		event.preventDefault();
		$("#content-table").slideUp("slow");
		$("#add-program-form").slideDown("slow");
    },
    'click #cancel-add': function (event) {
    	event.preventDefault();
        $("#edit-program-form").slideUp("slow");
    	$("#add-program-form").slideUp("slow");
		$("#content-table").slideDown("slow");
    },
    'submit #add-program-form': function(event,template) {
        //insertUrlVideoYoutube($("#inputUrlVideo").val() , $("#inputNameVideoYouTube").val(), $("#inputCategorieVideoYouTube").val() ,$("#inputCompanyVideoVideoYouTube").val(),$("#inputDescriptionVideoYouTube").val());
        event.preventDefault();
        var date = dateString()
        var data = 	{	
        				name:$('#name').val(),
                        video:$('#url-video').val(),
        				enviroment:$('#enviroment').val(),
                        nameEnviroment:$("#enviroment option:selected").text(),
        				description:$('#description').val(),
        				user:Meteor.userId(),
        				createAt:dateString()["stringDate"],
                        day: date["day"],
                        month: date["month"], 
                        year: date["year"],
                        time: date["time"],
        				img:'',
        				status:true,
        				
    				}
    	addProgram(data)
    },
    'submit #edit-program-form': function(event,template) {
        
        event.preventDefault();
        var data =  {   
                        _id:$("#id-program-edit").val(),
                        name:$('#edit-name').val(),
                        video:$('#edit-url-video').val(),
                        enviroment:$('#edit-enviroment').val(),
                        nameEnviroment:$("#edit-enviroment option:selected").text(),
                        description:$('#edit-description').val(),
                        dateModify :dateString()["stringDate"],
                        img:'',
                        user:Meteor.userId(),
                        status:true,
                    }
        editProgram(data)
    },
    'click .update-program': function (event) {
        event.preventDefault();
        $('.alert').remove()
        $("#content-table").slideUp("slow");
        $("#edit-program-form").slideDown("slow");
        loadDataEdit(event.currentTarget.id)
    }
})

Template.dashboard_programs.rendered = function(){
    setTimeout(function(){ 
		$('.alert').remove()
	}, 500);

}


function addProgram(data){
	$('.alert').remove()
    var fileImg = $('#img').get(0).files[0];
    var video_id = cutUrlDataYoutube(data.video);

    if (LimitAttach($('#img').val(),1) && isNotEmpty("#name",data.name) && isNotEmpty("#description",data.description) &&
    	isNotEmpty("#enviroment", data.enviroment) && video_id.status == true )
    {
        
		/*Upload Img CollectionFS*/
	    var newFileImg = new FS.File(fileImg);
	    newFileImg.metadata = {
	        owner: Meteor.userId()
	    };
	    var fileObjImg = Images.insert(newFileImg, function (err, fileObj) {});
	    /*Upload Img CollectionFS*/

	    data.img = fileObjImg;
        data.idVideoYoutube = video_id.videoId;

	    Meteor.call('insertProgram',data, function(err, res){
	        if (err){
	            var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Error, Intenta mas tarde</h4>';
	            $(liData).appendTo('.col-sm-offset-2').slideDown('slow');          
	        }
	    });
	           
    }else{
        var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">LLena todos los Campos Correctamente</h4>';
                    $(liData).appendTo('.col-sm-offset-2').slideDown('slow');    
    }
}

function editProgram(data){
    $('.alert').remove()
    
    var video_id = cutUrlDataYoutube(data.video);
    if (isNotEmpty("#name",data.name) &&  isNotEmpty("#description",data.description) && isNotEmpty("#enviroment", data.enviroment) && video_id.status == true )
    {
        data.idVideoYoutube = video_id.videoId;
        if ($('#edit-img').val() != '' ){
            if (LimitAttach($('#edit-img').val(),1) == true){

                var fileImg = $('#edit-img').get(0).files[0];
                /*Upload Img CollectionFS*/
                var newFileImg = new FS.File(fileImg);
                newFileImg.metadata = {
                    owner: Meteor.userId()
                };
                var fileObjImg = Images.insert(newFileImg, function (err, fileObj) {});
                /*Upload Img CollectionFS*/

                data.img = fileObjImg;
                Meteor.call('updateProgram',data, function(err, res){
                if (err){
                    sweetAlert("Ops..!", "Error, Intenta de Nuevo", "error")          
                }
            });

            }else{
                sweetAlert("Ops..!", "Rellena todos los campos correctamente", "error")
            };
        }else{
            Meteor.call('updateProgram',data, function(err, res){
                if (err){
                    sweetAlert("Ops..!", "Error, Intenta de Nuevo", "error")          
                }else{
                    sweetAlert("Buen Trabajo!", "Se han guardado los datos con éxito!", "success")
                }
            });
        }       
    }else{
        sweetAlert("Ops..!", "Rellena todos los campos correctamente", "error")
    }
}

function loadDataEdit(id){
    var data = Programs.findOne({_id:id});

    $("#edit-name").val(data.name);
    $("#edit-url-video").val(data.video);
    $("#edit-enviroment").val(data.enviroment)
    $("#edit-description").val(data.description)
    $("#edit-img-preview").attr("src","/cfs/files/images/"+data.img._id+"/"+data.img.original.name)
    $("#id-img-edit").val(data.img._id);
    $("#id-program-edit").val(id)
}

