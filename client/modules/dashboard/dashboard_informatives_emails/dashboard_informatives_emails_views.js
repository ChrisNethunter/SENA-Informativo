Template.dashboard_informatives_emails.helpers({
    showInformative: function(object) {
       return Informative.find();
    },
    showInformativeCount: function(object) {
       return Informative.find().count();
    },
    showEnviroments: function(object) {
       return Enviroments.find();
    },
    showPrograms:function(object){
       
        return Programs.find({enviroment:Session.get("enviroment")});
    },
    showImages:function(object){
        return Images.find();
    },
    tableSettings : function () {
        return {
                    rowsPerPage: 15,
                    fields: [
                        { 
                            key: 'img', 
                            label: 'Imagen',
                            fn: function (img, object) {
                                var img = "/cfs/files/images/"+img._id+"/"+img.original.name
                                var html =  '<img class="preview-img" id="img-enviroment" src="'+img+'">';
                                return new Spacebars.SafeString(html);
                            }
                        },
                        { 
                            key: 'title', 
                            label: 'Título' 
                        },
                        { 
                            key: 'description', 
                            label: 'Descripción' 
                        },
                        { 
                            key: 'subtitle', 
                            label: 'Subtítulo' 
                        },
                        { 
                            key: 'descriptiontwo', 
                            label: 'Subtítulo' 
                        },
                        { 
                            key: 'nameEnviroment', 
                            label: 'Ambiente' 
                        },
                        { 
                            key: 'nameProgram', 
                            label: 'Programa' 
                        },
                        
                        { 
                            key: 'createAt', 
                            label: 'Fecha de Creación' 
                        },
                        { 
                            key: 'status', 
                            label: 'Estado',
                            fn: function (estado, object) {
                                if (estado == "activo"){
                                    var html =  '<div id="true" class="btn btn-success btn-flat">Activo</div>';
                                    
                                }else{
                                    var html =  '<div id="false" class="btn btn-danger btn-flat">Inactivo</div>';
                                };
                                return new Spacebars.SafeString(html);
                                
                            }
                        },
                        { 
                            key: '_id', 
                            label: 'Opciones',
                            fn: function (_id, object) {
                                var html =  '<button id="'+_id+'" class="sent-informative btn btn-info btn-flat">Enviar Voletin</button>';
                                return new Spacebars.SafeString(html);
                            }
                        },     
                    ],
        };
    }

});
Template.dashboard_informatives_emails.onCreated(function () {
    Meteor.subscribe("enviroments");
    Meteor.subscribe("Images", Meteor.userId())
});