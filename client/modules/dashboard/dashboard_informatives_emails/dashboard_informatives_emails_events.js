var emails ="";

Template.dashboard_informatives_emails.rendered = function(){
    

    $(".content-users").fadeIn("slow");
    Session.set("skip",16)
    Session.get("enviroment","")
    $('.alert').remove()

    Meteor.call('countDataInformative',function(error, result) {
        if (!error){
        	subscription = Meteor.subscribe("informativeEmails",Session.get("skip"))
        	$("#count").text(result)
        }
    });
    
    

}
/*
function suscribesEmail(program){    
    var dataSubscribes = Subscribes.find({program:program}).fetch();
    for (var i=0; i< dataSubscribes.length; i++){
        emails += dataSubscribes[i].email + ",";
    }
    console.log(emails)
}*/

Template.dashboard_informatives_emails.events({

    'click #create-informative': function (event) {
		event.preventDefault();
		$("#content-table").slideUp("slow");
		$("#new-informative-item").slideDown("slow");
    },
    'click #cancel-add': function (event) {
    	event.preventDefault();
        //$("#edit-program-form").slideUp("slow");
    	$("#new-informative-item").slideUp("slow");
		$("#content-table").slideDown("slow");
    },
    'submit #new-informative-item': function (event) {
      	event.preventDefault();
      	var date = dateString()
      	var data = 	{
      					title:$("#title").val(),
      					description:$("#description").val(),
      					subtitle:$("#subtitle").val(),
      					descriptiontwo:$("#description-two").val(),
      					user:Meteor.userId(),
                        createAt:dateString()["stringDate"],
                        day: date["day"],
                        month: date["month"], 
                        year: date["year"],
                        time: date["time"],
                        img:'',
                        status:"activo",
      				}

      	if ($('#enviroment').val() != '' || $('#program').val() != '' ){
            data.enviroment = $('#enviroment').val()
            data.nameEnviroment = $("#enviroment option:selected").text()
            data.program = $('#program').val()
            data.nameProgram = $("#program option:selected").text()
        };	
        addInformative(data)
    	//email($("#title").val(),$("#subtitle").val(),$("#description").val())  
    },
    'change #enviroment':function(event){
    	event.preventDefault();
    	if ($(event.currentTarget).val() != ''){
    		Meteor.subscribe("programEnviroment",$(event.currentTarget).val())
    		Session.set("enviroment",$(event.currentTarget).val())
    		$(".sel-program").slideDown("slow");
    	}else{
    		$(".sel-program").slideUp("slow");
    		Session.set("enviroment","")
    	};
    },
    'click .next-page' : function(event,template){
        event.preventDefault();
        Session.set("skip",Session.get("skip") + 15)
        Meteor.subscribe("informativeEmails",Session.get("skip"))
    },
    'click .sent-informative': function (event) {
        event.preventDefault();
        sweetAlert({
	          title: "Estas seguro?",
	          text: "Enviar voletin Informativo ahora ?",
	          type: "warning",
	          showCancelButton: true,
	          confirmButtonColor: "#DD6B55",
	          confirmButtonText: "Si, enviar Voletin Informativo!",
	          closeOnConfirm: false
	    },function() {
	    	email(event.currentTarget.id)
	        
	    });
        
    } 
});


function addInformative(data){
	$('.alert').remove()
    var fileImg = $('#img').get(0).files[0];
    if (LimitAttach($('#img').val(),1) && isNotEmpty("#title",data.title) && isNotEmpty("#description",data.description) 
    	&& isNotEmpty("#subtitle",data.subtitle) && isNotEmpty("#description-two",data.descriptiontwo) == true )
    {
        
		/*Upload Img CollectionFS*/
	    var newFileImg = new FS.File(fileImg);
	    newFileImg.metadata = {
	        owner: Meteor.userId()
	    };
	    var fileObjImg = Images.insert(newFileImg, function (err, fileObj) {});
	    /*Upload Img CollectionFS*/
	    data.img = fileObjImg;
	    Meteor.call('insertInformative',data, function(err, res){
	        if (err){
	        	alert(err)
	            var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Error, Intenta mas tarde</h4>';
	            $(liData).appendTo('.col-sm-offset-2').slideDown('slow');      
	        }
	    });
	           
    }else{
        var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">LLena todos los Campos Correctamente</h4>';
                    $(liData).appendTo('.col-sm-offset-2').slideDown('slow');    
    }
}

function email(id){

	Meteor.call('serchDataInformative',id,function(err, res){
      if (!err){
      	var imgUrl = "https://192.168.0.6:3000/cfs/files/images/" + res.img._id + "/" + res.img.original.name;

        var html = ' <div style="background:#fff">\
				      <div style="max-width:500px;margin: 0 auto;">\
				        <div style="background:#238276;padding:50px;">\
				           <div style="text-align:center;font-family:verdana;">\
				             <h1 style="font-size: 2em;color: #fff;font-weight: bold;letter-spacing:.3em;text-transform:uppercase;">SENA CDITI</h1>\
				             <h2 style="padding:20px 0 0 0;color:white;">Centro de Diseño e Innovación Tecnológica Industrial</h2>\
				           </div>\
				        </div>\
				        <div style="padding:20px;color:#666;font-family: verdana;background: #ecf0f1;">\
				          <h1 style="font-size:1.2em;color: #121212;">'+ res.title +'</h1>\
				          <p style="line-height:1.5em;padding: 20px 0;">'+ res.description +'</p>\
				          <h2 style="color:#121212;font-size:1.2em;">'+ res.nameEnviroment + ' / ' + res.nameProgram + '</h2>\
				          <a href="#"><img style="width:100%;padding-top:20px;" src="http://3.bp.blogspot.com/-ocqcztvKKqA/VtSh4FKwMrI/AAAAAAAAETE/hpXlQwyF7OM/s1600-r/41_Blog-naranja-01.png" alt="" /></a>\
				          <p style="line-height:1.5em;padding: 20px 0;">'+ res.description +'</p>\
				          <h1 style="font-size:1.2em;color: #121212;">'+ res.subtitle+'</h1>\
				          <p style="line-height:1.5em;padding: 20px 0;">'+ res.descriptiontwo +'</p>\
				          <a href="http://industriadosquebradas.blogspot.com.co/" style="color:#fff;padding: 10px 20px;background:#fc7323;text-decoration: none;">Ver Más</a>\
				        </div>\
				        <div style="padding:20px;text-align:center;background:#59b548;color:white;">\
				          <p>visit site | unsubscribe</p>\
				        </div>\
				      </div>\
				    </div>';

        if (res.program != ''){
            var program = res.program
        }else{
            var program = ""
        };

		Meteor.call('emailPru',html, res.program,res.title,function(err, res){
	      if (!err){
	        sweetAlert("Buen Trabajo!", "Se ha enviado correctamente el correo informativo a los destinatarios subscritos", "success");
	      };
	  	});

      };
  	});
}

