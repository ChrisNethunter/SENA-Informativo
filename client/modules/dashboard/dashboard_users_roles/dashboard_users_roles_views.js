Template.dashboard_users_roles.helpers({
    showUsers: function(object) {
       return Meteor.users.find();
    },

    tableSettings : function () {
         return {
                    rowsPerPage: 15,
                    fields: [
                        { 
                            key: '_id', 
                            label: '' ,
                            fn: function (_id, object) {
                                var html =  '<input id="'+_id+'" class="check-user-role" type="checkbox" value=""/>';
                                return new Spacebars.SafeString(html);
                            }
                        },
                        { 
                            key: 'profile.name', label: 'Name' 
                        },
                        { 
                            key: 'emails.0.address', label: 'Email' 

                        },
                        { 
                            key: 'roles', 
                            label: 'Role',
                            fn: function (roles, object) {
                                var html =  '<select id="" class="role-user">\
                                                <option id="'+ roles +'" value="' + roles + '">'+ roles +'</option>\
                                                <option id="freeUser" value="freeUser">freeUser</option>\
                                                <option id="platinumUser" value="platinumUser">platinumUser</option>\
                                                <option id="goldUser" value="goldUser">goldUser</option>\
                                                <option id="admin" value="admin">Admin</option>\
                                            </select>';
                                return new Spacebars.SafeString(html);
                            }
                        },
                        {
                            key:'createdAt' ,label:'createdAt'
                        },
                       
                    ],
   
        };
    }

});
Template.dashboard_users_roles.onCreated(function () {
    Meteor.subscribe('users')
    
});