Template.dashboard_users_roles.rendered = function(){
    $(".content-users").fadeIn("slow");

}

Template.dashboard_users_roles.events({
	'click #changes-users-role': function (event) {
        changeRoles();
    },
    'click #create-user': function (event) {
        event.preventDefault();
        $("#content-table").slideUp("slow");
        $("#add-user").slideDown("slow");
    },
    'click #cancel-add': function (event) {
        event.preventDefault();
        $("#add-user").slideUp("slow");
        $("#content-table").slideDown("slow");      
    },
    'submit #add-user': function(event, template) {
        event.preventDefault();
        createUser($('#name').val(),$('#cc').val(),$('#email').val());
    },
});

function changeRoles(){
    console.log("code run")
	$(".check-user-role").each(function(){

        if ($(this).is(':checked')) {
        	var data ={
        		uId: $(this).attr('id'),
        		rol: $(this).parents('tr').find('select').val()
        	};

            console.log(JSON.stringify(data))
        	Meteor.call('updateRoleXToUser',data, function(error,result){
        		if (error){
                    sweetAlert('Oops...', 'Intenta de Nuevo!', 'error');
                }else{
                    sweetAlert("Buen Trabajo!", "Se han guardado los cambios de roles de usuario!", "success");
                };
            });
        	
        	//console.log(u);
        };
    })
}
function createUser(name,cc,email){

  if (isNotEmpty("#name",name) && validateNumber("#cc",cc) && validateEmail("#email",email) === true){

    data = {
      createdBy: Meteor.userId(),
      name:name,
      cc:cc,
      status:"activo",
      pass:generatePass(8),
      email:email,
      createAt:dateString()["stringDate"],
    }
  
    Meteor.call('createUserPlataform',data, function(err, res){

      if(typeof(res) == 'string'){

        idUser = res;
        Meteor.call('setRoleUserStandard',idUser, function(err, res){
          if (err){
            sweetAlert('Oops...', 'Intenta de Nuevo!', 'error');
          }else{
            sendEmail(data.email,data.pass)
            sweetAlert("Buen Trabajo!", "Se ha creado un nuevo Usuario!", "success");
          };
        });
      }else if( res == 2 ){

        sweetAlert('Oops...', 'Este Usuario ya esta registrado!', 'error');
      }
    });
  }else{
    sweetAlert('Oops...', 'Rellena todos los Campos Correctamente!', 'error');
    
  };
}

function sendEmail(toEmail,pass){ 

  Meteor.call('sendEmailUserCreated',Meteor.user().emails.address,toEmail,pass, function(err, res){
    if (err){
      sweetAlert('Oops...', 'No se ha podido enviar Email al Remitente!', 'error');
    }else{
      sweetAlert("Buen Trabajo!", "Se ha enviado un Email al Nuevo Usuario!", "success");
    
    };
  });
}