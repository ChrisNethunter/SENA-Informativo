Template.informative_footer.rendered = function() {
	setTimeout(function(){ 
		$('#google-map').gMap({
	        address: 'Diag. 27A, Dosquebradas, Risaralda, Colombia',
	        maptype: 'ROADMAP',
	        zoom: 16,
	        markers: [

	            {
	                address: "Diag. 27A, Dosquebradas, Risaralda, Colombia",
	                html: '<div style="width: 300px;"><h4 style="margin-bottom: 8px;">SENA CDITI</span></h4><p class="nobottommargin">Eres Bienvenido</p></div>',
	                icon: {
	                    image: "img/informative/map_pin2.png",
	                    iconsize: [40, 47],
	                    iconanchor: [40,47]
	                }
	            }
	        ],
	        	doubleclickzoom: false,
	        	controls: {
	            panControl: true,
	            zoomControl: true,
	            mapTypeControl: true,
	            scaleControl: false,
	            streetViewControl: false,
	            overviewMapControl: false
	        }

	    }); 
	}, 1000);
 	 
};