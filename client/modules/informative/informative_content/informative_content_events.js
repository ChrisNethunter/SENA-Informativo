Template.informative_content.rendered = function() {
	Session.set("statusData",0)
 	$(".demo1 .rotate").textrotator({
	    animation: "flipUp",
	    speed: 4000
	});
	// Preloader
	$('#status').fadeOut();
	$('#preloader').delay(350).fadeOut('slow');
	// jQuery Sticky menu
    $(window).scroll(function(){
	    if ($(window).scrollTop() > 10){
	        $('.main-nav').addClass("sticky");
	    }else{
	        $('.main-nav').removeClass("sticky");
	    }
	});
	// Off-screen Navigation
 	$('.navbar-toggle, nav').click(function(){
        $('.navbar-toggle').toggleClass('navbar-on');
        $('nav').fadeToggle();
        $('nav').removeClass('nav-hide');
    });
    $('.main-nav').removeClass("sticky");  

     
};


Template.informative_content.events({

	'click #overlay-hello': function (event) {
		event.preventDefault();
		$("#overlay-hello").fadeOut()
    },
});

    