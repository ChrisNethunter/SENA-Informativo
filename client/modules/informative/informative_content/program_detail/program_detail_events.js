Template.programs_detail.rendered = function() {

 	setTimeout(function(){
 		document.body.scrollTop = 0;
 		$('#status').fadeOut();
		$('#preloader').delay(350).fadeOut('slow');
 		$('.main-nav').addClass("sticky");
 	}, 1000);
 	$(window).scroll(function(){
	    if ($(window).scrollTop() < 10){
	        $('.main-nav').addClass("sticky");
	    }
	}); 
};

Template.programs_detail.events({
	'click #subscribe-program' : function(event, template){
	  event.preventDefault();
	  $("#subscribe-program").fadeOut()
	  $("#subscribe-form").fadeIn("slow")
	},

	'submit #subscribe-form': function(event,template) {
        //insertUrlVideoYoutube($("#inputUrlVideo").val() , $("#inputNameVideoYouTube").val(), $("#inputCategorieVideoYouTube").val() ,$("#inputCompanyVideoVideoYouTube").val(),$("#inputDescriptionVideoYouTube").val());
        event.preventDefault();
        var date = dateString()
        var data = 	{	
        				email:$('#email').val(),
                        enviroment:$("#enviroment").val(),
                        nameEnviroment:$("#nameEnviroment").val(),
                        program:$("#program").val(),
                        nameProgram:$("#nameProgram").val(),
        				createAt:dateString()["stringDate"],
                        day: date["day"],
                        month: date["month"], 
                        year: date["year"],
                        time: date["time"],
        				status:true,
    				}
    	subscribeUser(data)
    },
});

function subscribeUser(data){
	if (validateEmail("#email",data.email) == true ){


		Meteor.call('subscribeEmail',data, function(err, res){
            if (err){
                sweetAlert("Ops..!", "Error, Intenta de Nuevo", "error")          
            }else{
                sweetAlert("Buen Trabajo!", "Te has inscrito al voletin informativo correctamente", "success")
            }
        });
		
	}else{
        sweetAlert("Ops..!", "Ingresa un correo electrónico válido", "error")
    };
}