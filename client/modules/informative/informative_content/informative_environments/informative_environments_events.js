Template.informative_environments.rendered = function() {

  
};

Template.informative_environments.events({
	'keyup #search-box': function (event) {

      //event.preventDefault();
      Session.set("filterPrograms",$("#search-box").val());
      $('#filter-programs').css('display','none');
      $("#filter-programs").fadeIn("slow");

    },
    'click .filter': function (event) {
  		event.preventDefault();
  		
      
  		if (event.currentTarget.id == 'all'){

  			Session.set("filterEnviroment",'');
        $('#detail-enviroment').css('display','none');
  			$('#reactive-table-1-filter').css('display','none');
        $("#detail-enviroment").fadeOut()
  		}else{
  			Session.set("filterEnviroment",event.currentTarget.id)
        $("#detail-enviroment").fadeIn("slow")
  			$("#reactive-table-1-filter").fadeIn("slow");
        
  		};
  		$('#filter-programs').css('display','none');
  		$("#filter-programs").fadeIn("slow");
		
    },

    'click .program-detail': function (event) {
      event.preventDefault();
      //alert(event.currentTarget.id)
      Router.go("programDetail",{_ProgramId: event.currentTarget.id})
    
    },
    'click #video': function (event) {
      event.preventDefault();
        $("#conten-enviroment").slideUp("slow")
        $("#video-enviroment").slideDown("slow")
        $("#video").hide()
        $("#content").show()    
    },
    'click #content': function (event) {
      event.preventDefault();
        $("#video-enviroment").slideUp("slow")
        $("#conten-enviroment").slideDown("slow")
        $("#content").hide()
        $("#video").show()    
    },

    
})


