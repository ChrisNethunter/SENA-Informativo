Template.informative_events.rendered = function() {
 
};

Template.informative_events.events({
	'click .event-se': function (event) {
		event.preventDefault();

		var src = $(event.currentTarget).find("img").attr('src')
		var title = $(event.currentTarget).find("h5").text()
		var description = $(event.currentTarget).find("p").text()
		$("#img-eve").attr('src',src)
		$("#title-eve").text(title)
		$("#description_eve").text(description)
		

		$("#overlay-hello").fadeIn()
    },
});