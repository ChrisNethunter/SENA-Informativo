"use strict";		
$(window).load(function() {

	setTimeout(function(){ 
		$('.main-nav').addClass("sticky");
		// Smooth Scroll Navigation

    	$('.local-scroll').localScroll({offset: {top: -70},duration: 1500,easing:'easeInOutExpo'});

    	$(window).scroll(function(event){
			var scroll = $(window).scrollTop();
			if (scroll >= 50) {
			    $("#back-to-top").addClass("show");
			} else {
			    $("#back-to-top").removeClass("show");
			}
		});

		$('a[href="#top"]').on('click',function(){
		    $('html, body').animate({scrollTop: 0}, 'slow');
		    return false;
		});
		

	},2000);

	
});
	

