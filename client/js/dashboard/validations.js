/*$(window).load(function() {
    $('body').addClass('login-content');
    
});*/

validateEmail = function ( nameInput,email ) {
  expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if ( !expr.test(email) ){
      $(nameInput).css({"background":"#f2dede","border-color": "#f2dede","color":"#f44336","border-radius":"5px"}).val("This field must be a valid email");  
  }else{
      $(nameInput).css({"background":"white","color":"black","border-radius":"5px","border-color": "white"});  
      return true
  }
}
validateNumber = function (nameInput,f)  {
  if (isNaN(f)) {
    $(nameInput).css({"background":"#f2dede","border-color": "#f2dede","color":"#f44336","border-radius":"5px"}).val("This field must have only numbers"); 
    return false;
  }else{
    $(nameInput).css({"background":"white","color":"black","border-radius":"5px","border-color": "white"});  
    return true;
  }
}

isUrl = function(nameInput,s) {    
    var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
    if (regexp.test(s) == true){
      console.log("hey validate url true  -> " + nameInput)
      $(nameInput).css({"background":"white","color":"black","border-radius":"5px","border-color": "white"});  
      return true;
    }else{
      console.log("hey validate url false -> " + nameInput)
      $(nameInput).css({"background":"#f2dede","border-color": "#f2dede","color":"#f44336","border-radius":"5px"}).val("This field must be a valid URL"); 
      return false;
    };
    
}

validatePassword = function (fld) {
    var error = "";
    var illegalChars = /[\W_]/; // allow only letters and numbers
 
    if (fld.value == "") {
        fld.style.background = 'Yellow';
        error = "You didn't enter a password.\n";
        alert(error);
        return false;
 
    } else if ((fld.value.length < 7) || (fld.value.length > 15)) {
        error = "The password is the wrong length. \n";
        fld.style.background = 'Yellow';
        alert(error);
        return false;
 
    } else if (illegalChars.test(fld.value)) {
        error = "The password contains illegal characters.\n";
        fld.style.background = 'Yellow';
        alert(error);
        return false;
 
    } else if ( (fld.value.search(/[a-zA-Z]+/)==-1) || (fld.value.search(/[0-9]+/)==-1) ) {
        error = "The password must contain at least one numeral.\n";
        fld.style.background = 'Yellow';
        alert(error);
        return false;
 
    } else {
        fld.style.background = 'White';
    }
   return true;
}

isNotEmpty = function(nameInput,value) {
    if (value && value !== ''){
        $(nameInput).css({"background":"white","color":"black","border-radius":"5px","border-color": "white"});  
        return true;
    }else{
        $(nameInput).css({"background":"#f2dede","border-color": "#f2dede","color":"#f44336","border-radius":"5px"}).val("This field can not be empty");  
        return false;
    }
};



dateString = function (){ 
  var months = new Array ("January","February","March","April","May","June","July","August","September","October","November","December");
  var weekDays = new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
  var f=new Date();
  dateArr = {
              stringDate: weekDays[f.getDay()] + "-" + f.getDate() + "-" + months[f.getMonth()] + "-" + f.getFullYear(),
              day: weekDays[f.getDay()],
              month: months[f.getMonth()], 
              year: f.getFullYear()
            }
  return(dateArr);

}

validaFechaDDMMAAAA = function(fecha){
  var dtCh= "/";
  var minYear=1900;
  var maxYear=2100;

  function isInteger(s){
    var i;
      for (i = 0; i < s.length; i++){   
          // Check that current character is number.
          var c = s.charAt(i);
          if (((c < "0") || (c > "9"))) return false;
      }
      // All characters are numbers.
      return true;
  }

  function stripCharsInBag(s, bag){
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
  }
    return returnString;
}

function daysInFebruary (year){
  // February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
  for (var i = 1; i <= n; i++) {
    this[i] = 31
    if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
    if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
  var daysInMonth = DaysArray(12)
  var pos1=dtStr.indexOf(dtCh)
  var pos2=dtStr.indexOf(dtCh,pos1+1)
  var strDay=dtStr.substring(0,pos1)
  var strMonth=dtStr.substring(pos1+1,pos2)
  var strYear=dtStr.substring(pos2+1)
  strYr=strYear
  if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
  if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
  for (var i = 1; i <= 3; i++) {
    if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
  }
  month=parseInt(strMonth)
  day=parseInt(strDay)
  year=parseInt(strYr)
  if (pos1==-1 || pos2==-1){
    alert("The date format should be : dd/mm/yyyy")
    return false
  }
  if (strMonth.length<1 || month<1 || month>12){
    alert("Please enter a valid month")
    return false
  }
  if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
    alert("Please enter a valid day")
    return false
  }
  if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
    alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
    return false
  }
  if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
    alert("Please enter a valid date")
    return false
  }
return true
}
  if(isDate(fecha)){
    return true;
  }else{
    return false;
  }
}

dateString = function (){ 
  var months = new Array ("January","February","March","April","May","June","July","August","September","October","November","December");
  var weekDays = new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
  var f=new Date();
  dateArr = {
              stringDate: weekDays[f.getDay()] + "-" + f.getDate() + "-" + months[f.getMonth()] + "-" + f.getFullYear(),
              day: weekDays[f.getDay()],
              month: months[f.getMonth()], 
              year: f.getFullYear()
            }
  return(dateArr);

}

validaFechaDDMMAAAA = function(fecha){
  var dtCh= "/";
  var minYear=1900;
  var maxYear=2100;

  function isInteger(s){
    var i;
      for (i = 0; i < s.length; i++){   
          // Check that current character is number.
          var c = s.charAt(i);
          if (((c < "0") || (c > "9"))) return false;
      }
      // All characters are numbers.
      return true;
  }

  function stripCharsInBag(s, bag){
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
  }
    return returnString;
}

function daysInFebruary (year){
  // February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
  for (var i = 1; i <= n; i++) {
    this[i] = 31
    if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
    if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
  var daysInMonth = DaysArray(12)
  var pos1=dtStr.indexOf(dtCh)
  var pos2=dtStr.indexOf(dtCh,pos1+1)
  var strDay=dtStr.substring(0,pos1)
  var strMonth=dtStr.substring(pos1+1,pos2)
  var strYear=dtStr.substring(pos2+1)
  strYr=strYear
  if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
  if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
  for (var i = 1; i <= 3; i++) {
    if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
  }
  month=parseInt(strMonth)
  day=parseInt(strDay)
  year=parseInt(strYr)
  if (pos1==-1 || pos2==-1){
    alert("The date format should be : dd/mm/yyyy")
    return false
  }
  if (strMonth.length<1 || month<1 || month>12){
    alert("Please enter a valid month")
    return false
  }
  if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
    alert("Please enter a valid day")
    return false
  }
  if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
    alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
    return false
  }
  if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
    alert("Please enter a valid date")
    return false
  }
return true
}
  if(isDate(fecha)){
    return true;
  }else{
    return false;
  }
}

dateString =  function (){ 
    var months = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    var weekDays = new Array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
    var f=new Date();
    dateArr = {
                stringDate: weekDays[f.getDay()] + "-" + f.getDate() + "-" + months[f.getMonth()] + "-" + f.getFullYear(),
                day: weekDays[f.getDay()],
                month: months[f.getMonth()], 
                year: f.getFullYear(),
                time : f.getHours()+":"+f.getMinutes()+":"+f.getSeconds(),
              }
    return(dateArr);
}

generatePass = function(L){
  var s= '';
  var randomchar=function(){
    var n= Math.floor(Math.random()*62);
    if(n<10) return n; //1-10
    if(n<36) return String.fromCharCode(n+55); //A-Z
    return String.fromCharCode(n+61); //a-z
  }
  while(s.length< L) s+= randomchar();
  return s;
}

LimitAttach = function (tField,iType) 
{   
    file=tField;
    if (iType==1) { 
        extArray = new Array(".gif",".jpg",".png","JPEG");
    }
    if (iType==2) {
        extArray = new Array(".swf");
    }
    if (iType==3) {
        extArray = new Array(".exe",".sit",".zip",".tar",".swf",".mov",".hqx",".ra",".wmf",".mp3",".qt",".med",".et");
    }
    if (iType==4) {
        extArray = new Array(".mov",".ra",".wmf",".mp3",".qt",".med",".et",".wav",".mp4", ".ogv", ".ogg", ".webm" ,".avi");
    }
    if (iType==5) {
        extArray = new Array(".html",".htm",".shtml");
    }
    if (iType==6) {
        extArray = new Array(".doc",".xls",".ppt");
    }
    if (iType==7) {
        extArray = new Array(".txt",".htm",".html",".pdf",".swf");
    }


    allowSubmit = false;
    if (!file) return;

    while (file.indexOf("\\") != -1) file = file.slice(file.indexOf("\\") + 1);
    ext = file.slice(file.indexOf(".")).toLowerCase();
    for (var i = 0; i < extArray.length; i++) {
        if (extArray[i] == ext) {
            return true;
        }
    }

    if (!allowSubmit) 
    {
        alert("Usted sólo puede subir archivos con extensiones " + (extArray.join(" ")) + "\nPor favor seleccione un nuevo archivo");
    }
}

/*YouTube Get Video Info*/
cutUrlDataYoutube = function (urlYoutube){
  var url = urlYoutube;
  var videoid = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
  if(videoid != null) {
              data =  {
                          status:true,
                          videoId:videoid[1],
                      } 
     return data;
  } else {
      swal("Error!","No es una url de Youtube Válida", "error"); 
      return data =   {
                          status:false
                      }
      
  }
}