Router.configure({
	layoutTemplate: 'informative_layout',

});

Router.map(function(){

	this.route('Index', {
		path: '/',
		layoutTemplate: 'informative_layout',
	});

	/*dashboard*/
	this.route('login', {
		path: '/login',
		layoutTemplate: 'dashboard_signin',
	});

	this.route('singUp', {
		path: '/singUp',
		layoutTemplate: 'signUp',
	});	

	this.route('dashboard_control', {
		path: '/dashboard',
		layoutTemplate: 'dashboard_control',
	});	

	this.route('dashboardUsersRoles', {
		path: '/dashboard/users/roles',
		layoutTemplate: 'dashboard_control',
	});	

	this.route('dashboardInstitute', {
		path: '/dashboard/institucion',
		layoutTemplate: 'dashboard_control',
	});

	this.route('dashboardNews', {
		path: '/dashboard/noticias',
		layoutTemplate: 'dashboard_control',
	});
	this.route('dashboardEvents', {
		path: '/dashboard/eventos',
		layoutTemplate: 'dashboard_control',
	});
	this.route('dashboardEnviroments', {
		path: '/dashboard/ambientes',
		layoutTemplate: 'dashboard_control',
	});		

	this.route('dashboardSubscribes', {
		path: '/dashboard/subscribes',
		layoutTemplate: 'dashboard_control',
	});

	this.route('dashboardPrograms', {
		path: '/dashboard/programas',
		layoutTemplate: 'dashboard_control',
	});

	this.route('dashboardInformativeEmails', {
		path: '/dashboard/voletines',
		layoutTemplate: 'dashboard_control',
	});

	//*Client*//

	this.route('programDetail', {
		path: '/program/:_ProgramId',
		layoutTemplate: 'programs_detail',
		waitOn	: function () {
		    Meteor.subscribe('programDetail', this.params._ProgramId);	
		},
		data:function(){
			return Programs.findOne(this.params._ProgramId); 
		},
	});

	this.route('dashboardUsers', {
		path: '/dashboard/users',
		waitOn	: function () {
			if(Meteor.user()){
				Meteor.subscribe('users');
			}
		},
		layoutTemplate: 'dashboard_users_roles',
	});

	this.route('404', {
       path: '/*',
       layoutTemplate: 'pageNotFound',
       onBeforeAction: function(){
          console.log('not found');
       },
       onAfterAction:function(){
			// The SEO object is only available on the client. 
			// Return if you define your routes on the server, too.
			SEO.set({
				title: "Dashboard - Not Found",
				meta : {
						'description': "Not Found"
				},
			});
		}
    });

	/*dashboard*/
});

