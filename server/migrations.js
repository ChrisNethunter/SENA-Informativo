Migrations = new Meteor.Collection('migrations');

Meteor.startup(function () {
	// Migrations for Users
    if (!Migrations.findOne({slug: 'users'})) {

		var users = [
		    {name:"Admin User",email:"social@vifuy.com", pass:"vifuyw4515991", cc:"1088314789", roles:['admin']},
		    {name:"Admin William ",email:"william@vifuy.com", pass:"w4515991", cc:"4515991", roles:['admin']},
		    {name:"Admin Alejo ",email:"alejandro@vifuy.com" ,pass:"vifuy1234" , cc:"1088315369",roles:['admin']},
			{name:"Admin Christian",email:"cristian@vifuy.com", pass:"vifuychobits" ,cc:"1088215360", roles:['admin']},
			{name:"Admin Pablo",email:"juciff8@gmail.com", pass:"vifuy1892" , cc:"4637823647", roles:['admin']},

		];
		
		_.each(users, function (user) {
		  var id;

		  id = Accounts.createUser({
		    email: user.email,
		    password: user.pass,
		    profile:{   name: user.name,
		    		    cc:user.cc 
		    		}
		  });

		  if (user.roles.length > 0) {
		    // Need _id of existing user record so this call must come 
		    // after `Accounts.createUser` or `Accounts.onCreate`
		    Roles.addUsersToRoles(id, user.roles);
		  }
		});

		Migrations.insert({slug: 'users'});

		console.log('Running migrations for users...');
	}

	smtp =  {
          username: 'social@vifuy.com',   // eg: server@gentlenode.com
          password: 'vifuyvifuy1234',   // eg: 3eeP1gtizk5eziohfervU
          server:   'smtp.gmail.com',  // eg: mail.gandi.net
          port: 465
        }

	process.env.MAIL_URL = 'smtp://' + encodeURIComponent(smtp.username) + ':' + encodeURIComponent(smtp.password) + '@' + encodeURIComponent(smtp.server) + ':' + smtp.port;

	console.log(process.env.MAIL_URL)

	// Migrations for ...

});