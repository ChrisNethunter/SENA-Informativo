Meteor.methods({

	setRoleToFreeUser : function(idU){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		var id= Meteor.user();
		Roles.addUsersToRoles(id, ['freeUser']);
	},
	setRoleToPlatinumUser : function(idU){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		var id= Meteor.user();
		Roles.addUsersToRoles(id, ['platinumUser']);
	},
	setRoleToGoldUser : function(idU){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		var id= Meteor.user();
		Roles.addUsersToRoles(id, ['goldUser']);
	},
	updateRoleToFreeUser : function(idU){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Roles.setUserRoles(idU, ['freeUser']);
	},
	updateRoleToPlatinumUser : function(idU){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Roles.setUserRoles(idU, ['platinumUser']);
	},
	updateRoleToGoldUser : function(idU){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Roles.setUserRoles(idU, ['goldUser']);
	},
	'createUserWithRole': function(data) {
	    var userId;
	    var role = ['freeUser'];
	    var email = data.email;

	    var user = Meteor.users.find({"profile.cc":data.profile.cc}).count()

	    if(user == 1){
	    	return 3
	    }else{
	    	Meteor.call('createUserNoRole', data, function(err, result) {
			      if (err) {
			        return err;
			      }
			      Roles.addUsersToRoles(result, role);
			      return userId = result;
			});
			  // we wait for Meteor to create the user before sending an email
			  Meteor.setTimeout(function() {
			    Accounts.sendVerificationEmail(userId);
			  }, 2 * 1000);
			return userId;
	    };    
	},

	'createUserPlataform': function(data){

		var user = Meteor.users.find({"profile.cc":data.cc}).count()
		if (user != 1){
			id = Accounts.createUser({
										email: data.email,
									    password: data.pass,
								    	profile:{ 
								    				name: data.name,
								    				cc: data.cc,
								    				createdBy: data.createdBy,
								    				status: data.status,
								    				request:"change-pass",
								    				email:data.email,
								    				createAt:data.createAt, 
								    			}
								    });
			return id
		}else{
			return 2
		};
	},

	setRoleUserStandard : function(idUser){
		Roles.addUsersToRoles(idUser, ['standard']);
	},

	'createUserNoRole': function(data) {
	    //Do server side validation
	    return Accounts.createUser({
	      email: data.email,
	      password: data.password,
	      profile: data.profile
	    });

	},
	updateRoleXToUser : function(data){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }else{
	    	var dataUser = Meteor.users.findOne({_id:Meteor.userId()}) 
	    	if (dataUser.roles == "admin"){
	    		Roles.setUserRoles(data.uId,data.rol);
	    	}else{
	    		throw new Meteor.Error("not-authorized");
	    	};
	    }
		
		//return ('it\'s done boy');
	},

	'insertNews': function(metadata) {
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Events.insert(metadata)
        	    
	},

	/*Institute*/
	'insertInfoInstitute': function(metadata) {
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Entity.insert(metadata)
        	    
	},

	'editInfoInstitute':function(id,data){

		if(! Meteor.userId()){
			throw new Meteor.Error("not-authorized")
		}

	    var verifyUser = Entity.findOne({_id:id})

	    if (verifyUser.user == Meteor.userId()) {

	      	if (data.img == ''){
				Entity.update({'_id':id},{$set:{"name":data.name, 
												"email":data.email,
												"address":data.address,
                                                "phone":data.phone,
                                                "description":data.description,
                                            }});
			}else{

				Entity.update({'_id':id},{$set:{"name":data.name, 
												"email":data.email,
												"address":data.address,
                                                "phone":data.phone,
                                                "description":data.description,
                                                "img":data.img
                                            }});
			};
	    }else{
	    	throw new Meteor.Error("not-authorized");
	    }
	},

	'removeImgInstitute':function(id){
		if(! Meteor.userId()){
			throw new Meteor.Error("not-authorized")
		}

	    var verifyUser = Images.findOne({_id:id})

	    if (verifyUser.metadata.owner == Meteor.userId()){
	    	Images.remove({_id:id});	
	    };
	},
	/*Institute*/

	/*Programs*/
	'insertProgram': function(metadata) {
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Programs.insert(metadata)
        	    
	},

	'updateProgram': function(data) {
	    //Do server side validation
	    if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
	    if (data.img == ''){
	    	
	    	Programs.update({'_id':data._id},{$set:{
	    												"name":data.name,
	    												"video":data.video ,
	    												"enviroment":data.enviroment,
	    												"nameEnviroment":data.nameEnviroment,
	    												"description":data.description,
	    												"dateModify":data.dateModify,
	    												"idVideoYoutube":data.idVideoYoutube,
			}});
	    }else{
	    	
	    	Programs.update({'_id':data._id},{$set:{	
	    												"name":data.name,
	    												"video":data.video ,
	    												"enviroment":data.enviroment,
	    												"nameEnviroment":data.nameEnviroment,
	    												"description":data.description,
	    												"dateModify":data.dateModify,
	    												"idVideoYoutube":data.idVideoYoutube,
	    												"img":data.img
			}});
	    };

	},

	'searchProgramInfo': function(id) {
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		return Programs.findOne({_id:id})
        	    
	},
	/*Programs*/

	/*Enviroments*/
	'insertEnviroment': function(metadata) {
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Enviroments.insert(metadata)
        	    
	},
	/*Enviroments*/
	'searchEnviromentInfo': function(id) {
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		return Enviroments.findOne({_id:id})
        	    
	},

	'updateEnviroment': function(data) {
	    //Do server side validation
	    if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
	    if (data.img == ''){
	    	
	    	Enviroments.update({'_id':data._id},{$set:{
	    												"name":data.name,
	    												"video":data.video ,
	    												"description":data.description,
	    												"dateModify":data.dateModify,
	    												"idVideoYoutube":data.idVideoYoutube,
			}});
	    }else{
	    	
	    	Enviroments.update({'_id':data._id},{$set:{	
	    												"name":data.name,
	    												"video":data.video ,
	    												"description":data.description,
	    												"dateModify":data.dateModify,
	    												"idVideoYoutube":data.idVideoYoutube,
	    												"img":data.img
			}});
	    };

	},

	/*Enviroments*/

	sendEmailUserCreated:function(AdminEmail,to,pass){
		Email.send({
		  from: AdminEmail,
		  to: to,
		  subject: "Cuenta Creada como Usuario de apoyo",
		  text: "Has sido registrado/a como Usuario/a. Accede sena-informativo.meteor.com/dashboard con los siguientes datos Usuario: tu correo electrónico. contraseña: " + pass
		});
	},

	/*Subscribe User*/
	subscribeEmail:function(data){

		Subscribes.insert(data)
	},

	removeSubscribe:function(id){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Subscribes.remove({_id:id})
	},
	/*Subscribe User*/
	countDataNews:function(){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Events.find().count()
	},

	'updateNew': function(data) {
	    //Do server side validation
	    if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
	    if (data.img == ''){
	    	
	    	Events.update({'_id':data._id},{$set:{
	    												"name":data.name,
	    												"video":data.video ,
	    												"description":data.description,
	    												"dateModify":data.dateModify,
	    												"idVideoYoutube":data.idVideoYoutube,
			}});
	    }else{
	    	
	    	Events.update({'_id':data._id},{$set:{	
	    												"name":data.name,
	    												"video":data.video ,
	    												"description":data.description,
	    												"dateModify":data.dateModify,
	    												"idVideoYoutube":data.idVideoYoutube,
	    												"img":data.img
			}});
	    };
	},
	'emailPru':function(html,program,subject){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		if (program != ''){
			
			var dataSubscribes = Subscribes.find({program:program}).fetch();
			var emails = "";
		    for (var i=0; i< dataSubscribes.length; i++){

		        emails += dataSubscribes[i].email + ",";
		    }

			Email.send({
					from: "leon140294@outlook.com",
					to: emails,
					subject: subject,
					text: "Cosas para html",
					html:html

			});
		}else{

			var dataSubscribes = Subscribes.find().fetch();
			var emails = "";

		    for (var i=0; i< dataSubscribes.length; i++){

		        emails += dataSubscribes[i].email + ",";
		    }

			Email.send({
					from: "leon140294@outlook.com",
					to: emails,
					subject: subject,
					text: "Cosas para html",
					html:html

			});

		};
		
	},
	'sendComment':function(subject,html){

		var findEmailAdmin = Entity.findOne()

		Email.send({
						from: "leon140294@outlook.com",
						to: findEmailAdmin.email,
						subject: "Comentario de " + subject,
						text: "Comentario Plataforma Informativa",
						html:html

					});
						
	},
	'countDataInformative':function(html){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
	    return Informative.find().count()
	},

	'insertInformative':function(data){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
	    Informative.insert(data)
	},

	'serchDataInformative':function(id){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
	    return Informative.findOne({_id:id})
	},
	'dataEmail':function(program){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
	    var data = Subscribes.find({program:program});
	    return data
		
	},

	
})
