//process.env.MAIL_URL="smtp://social%40vifuy.com:vifuyvifuy1234@smtp.gmail.com:465/"; 

/*Accounts.config({
  sendVerificationEmail: true,
  forbidClientAccountCreation: false,
});*/
/*-----------------------------------Player-----------------------------------*/

/*user*/
Meteor.publish('user', function(user){
  return Meteor.users.find({_id:user});
});
Meteor.publish('users', function(user){
  return Meteor.users.find();
});

/*Dashboard User*/
Meteor.publish('Images', function(user){
  return Images.find({'metadata.owner':user});
});

Meteor.publish('imageEntity', function(id){
  return Images.find({_id:id});
});

Meteor.publish('entityUser', function(user){
  return Entity.find({user:user});
});

Meteor.publish('entity', function(){
  return Entity.find();
});

Meteor.publish('programs', function(user){
  return Programs.find({user:user});
});


Meteor.publish('enviroments', function(user){
  return Enviroments.find();
});

Meteor.publish('enviromentsDetail', function(id){
  return Enviroments.find({_id:id});
});

Meteor.publish('filterPrograms', function(id){
	return Programs.find();
});

Meteor.publish('susbcritions', function(){
  return Subscribes.find();
});

Meteor.publish('programEnviroment', function(enviroment){
  return Programs.find({enviroment:enviroment});
});

Meteor.publish('informativeEmails', function(skipCount){
  if (skipCount > 0){
    return Informative.find({},{limit: skipCount});
  }else{
    return this.stop();
  };

});

Meteor.publish('typeEvents', function(type,limit){
  return Events.find({type:type},{limit:limit});
});


/*Dashboard User*/

/*Informative*/
Meteor.publish('programsInfo', function(user){
  return Programs.find();
});

Meteor.publish('programDetail', function(id){
  return Programs.find({_id:id})
})

/*Informative*/
Meteor.publish('dataNews', function(type,skipCount){
  if (skipCount > 0){
    return Events.find({type:type},{limit: skipCount})
  }else{
    return this.stop();
  };
});

Meteor.publish('programsLimit', function(skipCount){
  if (skipCount > 0){
    return Programs.find({},{limit: skipCount})
  }else{
    return this.stop();
  };
});

Meteor.publish('enviromentsLimit', function(skipCount){
  if (skipCount > 0){
    return Enviroments.find({},{limit: skipCount})
  }else{
    return this.stop();
  };
});
